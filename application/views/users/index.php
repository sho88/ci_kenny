<!DOCTYPE html>

<html>
	<head>
		<title>CodeIgniter App - <?php echo $author ?></title>
		<?php $this->load->view('includes/meta_and_style') ?>
	</head>

	<body>
		<main class="users">
			<h1>Users</h1>
			<div class="users__form">
			<?php echo form_open('', ['method' => 'post']) ?>
				<div>
					<?php echo form_input(['autocomplete' => 'off', 'name' => 'forename', 'placeholder' => 'Forename', 'type' => 'text']) ?>
				</div>

				<div>
					<?php echo form_input(['autocomplete' => 'off', 'name' => 'surname', 'placeholder' => 'Surname', 'type' => 'text']) ?>
				</div>

				<div>
					<?php echo form_button(['name' => 'submit', 'type' => 'submit', 'content' => 'Add']) ?>
				</div>
			<?php echo form_close() ?>
			</div>

			<div class="users__list">
				<?php foreach ($users as $user) : ?>
					<div>
						<p><?php echo $user->forename . ' ' . $user->surname ?></p>
						<p><a href="/users/delete/<?php echo $user->id ?>">Delete</a></p>
					</div>
				<?php endforeach; ?>
			</div>
		</main>

		<footer>
			<div>
				<p>Created by <?php echo $author ?></p>
			</div>
		</footer>
	</body>
</html>

