<meta name="viewport" content="width=device-width, initial-scale=1" />
<style>
* { box-sizing: border-box; margin: 0; padding: 0; }
body { display: grid; grid-template-rows: 95vh 5vh; }

.users { padding: 20px; }
.users__form { padding: 10px 0; }
.users__form form { display: flex; flex-direction: column; gap: 10px; }
.users__form button, .users__form input { border: 0; outline: 0; padding: 10px; width: 100%; }
.users__form input { background: #f1f1f1; color: #d35400; }
.users__form input::placeholder { color: #aaa; }
.users__form button { background: #d35400; border: 0; border-radius: 30px; color: #ffffff; cursor: pointer; padding: 15px; }

@media (min-width: 512px) {
	.users__form button, .users__form input { width: auto; }
	.users__form button { padding: 10px; }
	.users__form form { flex-direction: row; }
}

.users__list {  }
.users__list > div { padding: 10px 0; }

footer { align-items: center; background: #ddd; color: #555; display: flex; justify-content: center; }
footer > div { text-align: right; }
footer > div > p { margin: 0; padding: 0; text-align: center; }
</style>