<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends KA_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library( 'users_lib' );
		$this->load->helpers( 'form' );
	}

	public function users( $id = null )
	{
		// if no id has been declared, then return all the users
		if ( ! is_null( $id ) ) {
			$this->_setResponse( $this->users_lib->get_user_by_id( $id )->getContents() );
			return;
		}
		
		// ...otherwise, display the user by ID
		$this->_setResponse( $this->users_lib->get_users()->getContents() );
	}

	public function environment()
	{
		echo "Environment variable 'APP_AUTHOR' is '{$this->env->get('APP_AUTHOR')}'";
	}

	public function form()
	{
		if ( $this->input->method() === 'post' )
		{
			$this->load->library( 'forms_lib' );
			$this->load->model( 'user_form' );

			// set the form and the input data
			$this->forms_lib->set_form( $this->user_form, $this->input->post() );

			// check if the form is valid
			$is_valid = $this->forms_lib->validate();

			// get the form
			if ($is_valid) {
				echo 'Success!';
			}
		}

		return $this->load->view('form/index');
	}

	// responsible for rendering the response brought from Guzzle
	private function _setResponse( $body )
	{
		return $this->output
			->set_content_type( 'application/json' )
			->set_output( $body );
	}

}
