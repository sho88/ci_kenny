<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends KA_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->library( 'users_lib' );
		$this->load->helper( 'form' );
	}

	public function index()
	{
		// for Cross-Site-Request-Forgery protection
		// Read more here: https://owasp.org/www-community/attacks/csrf
		// ...and more here: https://www.youtube.com/watch?v=eWEgUcHPle0
		$author = $this->env->get( 'APP_AUTHOR' );

		$csrf = [
      'name' => $this->security->get_csrf_token_name(),
      'hash' => $this->security->get_csrf_hash()
		];

		// get the users from the database
		$users = $this->users_lib->get_users_from_db();

		// if this is a "post" type of method, then create the user
		if ( $this->input->method() === 'post' ) {

			// if the user could not be created, then exit...
			$this->load->library( 'forms_lib' );
			$this->load->model( 'user_form' );

			// set the form and the input data
			$this->forms_lib->set_form( $this->user_form, $this->input->post() );

			// check if the form is valid
			if ( ! $this->forms_lib->validate() ) {
				exit('Form is not valid');
			}

			// if the user cannot be created
			if ( ! $this->users_lib->create_user( $this->input->post() ) ) {
				exit( 'Unable to add user, sorry' );
			}

			// otherwise, redirect the user back...
			return redirect('/users');
		}

		return $this->load->view( 'users/index', compact( 'author', 'csrf', 'users' ) );
	}

	public function delete( int $id = 0 )
	{
		// if the user id === '0' (no user id has been set), then redirect
		if ( $id === 0 ) {
			return redirect( '/users' );
		}

		// otherwise, delete the user by ID
		if ( ! $this->users_lib->delete_by_id( $id ) ) {
			exit( 'Unable to delete user. Please try again.' );
		}

		return redirect( '/users' );
	}

}