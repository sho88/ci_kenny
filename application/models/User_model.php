<?php

class User_model extends CI_Model {
	private $_table_name = 'users';

	public $id;
	public $forename;
	public $surname;

	public function __construct()
	{
		$this->load->database();
	}

	public function get()
	{
		$db = $this->db->get( $this->_table_name );
		return $db->result();
	}

	public function save()
	{
		return $this->db->insert( $this->_table_name, $this );
	}

	public function new_instance()
	{
		return new self();
	}

	public function get_by_id( $id )
	{
		$db = $this->db->get_where( $this->_table_name, [ 'id' => $id ]);
		return $db->row();
	}

	public function delete_by_id( $id )
	{
		return $this->db->delete( $this->_table_name, [ 'id' => $id ]);
	}
}