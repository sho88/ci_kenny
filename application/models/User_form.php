<?php

// there must be a way to include the KA form class...
// ...especially having after we following this:
// https://codeigniter.com/userguide3/general/core_classes.html
require_once APPPATH . '/core/KA_form.php';

class User_form extends KA_form {
	protected $properties = [
		'forename' => '',
		'surname' => '',
	];
	protected $rules = [
		['field' => 'forename', 'label' => 'Forename', 'rules' => 'trim|required'],
		['field' => 'surname', 'label' => 'Surname', 'rules' => 'trim|required']
	];
}

