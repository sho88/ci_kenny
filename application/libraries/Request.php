<?php defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;

class Request {

	public function __construct() 
	{
		$this->client = new GuzzleHttp\Client();
	}

	public function __call( $methodName, $args )
	{
		if ( ! method_exists( $this, $methodName ) && method_exists( $this->client, $methodName ) ) {
			return call_user_func_array( [$this->client, $methodName], $args );
		}
	}

}

