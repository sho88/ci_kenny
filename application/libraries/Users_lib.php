<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_lib {

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library( 'request' );
		$this->ci->load->model( 'user_model' );
	}

	public function create_user( array $data )
	{
		$user = $this->ci->user_model->new_instance();
		$user->forename = $data['forename'];
		$user->surname = $data['surname'];

		return $user->save();
	}

	public function delete_by_id( int $id = 0 )
	{
		$this->validate_id( $id );

		// if the user does not exist, then return "false"
		if ( ! $this->get_id( $id ) ) {
			return false;
		}

		// ...otherwise, delete and return...
		return $this->ci->user_model->delete_by_id( $id );
	}

	public function get_users()
	{
		$request = $this->ci->request->get( "https://jsonplaceholder.typicode.com/users" );
		if ( $request->getStatusCode() !== 200 ) {
			return false;
		}

		return $request->getBody();
	}

	public function get_user_by_id( int $id = 0 )
	{
		if ( $id === 0 ) {
			return false;
		}

		$request = $this->ci->request->get( "https://jsonplaceholder.typicode.com/users/$id" );
		if ( $request->getStatusCode() !== 200 ) {
			return false;
		}

		return $request->getBody();
	}

	public function get_users_from_db()
	{
		return $this->ci->user_model->get();
	}

	public function get_id( int $id = 0 )
	{
		$this->validate_id( $id );

		return $this->ci->user_model->get_by_id( $id );
	}

	protected function validate_id ( $id = 0 )
	{
		if ( $id === 0 ) {
			return false;
		}
	}

}