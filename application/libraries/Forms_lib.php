<?php defined('BASEPATH') OR exit('No direct script access allowed');

// there must be a way to include the KA form class...
// ...especially having after we following this:
// https://codeigniter.com/userguide3/general/core_classes.html
require_once APPPATH . '/core/KA_form.php';

class Forms_lib {
	protected $form = null;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library( 'form_validation' );
	}

	public function get_form()
	{
		return $this->form;
	}

	public function set_form( KA_form $form, array $data = [] )
	{
		$this->form = $form;

		if ( count( $data ) < 1 ) return;

		foreach ( $data as $key => $value ) {
			$this->form->set( $key, $value );
		}
	}

	public function validate()
	{
		return $this->ci->form_validation
			->set_rules( $this->form->get_rules() )
			->set_data( $this->form->get() )
			->run();
	}

}

