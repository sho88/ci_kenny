<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Dotenv\Dotenv; 

class Environment {
	public function __construct()
	{
		$this->dotenv = Dotenv::createImmutable(__DIR__ . '/../../');
		$this->dotenv->load();
	}

	public function get($key = null)
	{
		if (is_null($key)) throw new Exception('Environment variable not called...');
		return $_SERVER[$key];
	}
}