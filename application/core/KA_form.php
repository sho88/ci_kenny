<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KA_form extends CI_Model {

	public function get( $key = null ) 
	{
		return ( ! is_null($key) && isset($this->$key) ) ? $this->$key : $this->properties;
	}

	public function get_rules()
	{
		return $this->rules;
	}

	public function set( $key, $value )
	{
		if ( ! isset( $this->properties[$key] ) ) {
			return;
		}

		$this->properties[$key] = $value;
	}

}