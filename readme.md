# CodeIgniter Demo!

Hey Mr Kenny! Here is a dummy CodeIgniter app I developed. I created the ability to read the user, and delete. Maybe you could try updating the user via the method suggested. 


## To run the app
- Go to your terminal, and run the following commands:
```
$ git clone https://sho88@bitbucket.org/sho88/ci_kenny.git
$ cd ci_kenny
$ php composer install // this will install Guzzle and phpdotenv
$ mv .env_dummy .env // this will rename the ".env_dummy" file to ".env"
$ php -S localhost:8081 -t public_html
```

All the best!
Sho Carter
Email: <shosilva@icloud.com>